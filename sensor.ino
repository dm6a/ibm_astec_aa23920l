
#include <Adafruit_ADS1015.h>
#include <OneWire.h>
#include <DallasTemperature.h> //Temperatursensor OneWire
#include <BlueDot_BME280.h>

Adafruit_ADS1115 ads(0x48);                            //Aresse vom ADS1115 Modul

BlueDot_BME280 bme1;                                   //Object for Sensor 1
BlueDot_BME280 bme2;                                   //Object for Sensor 2

int bme1Detected = 0;                                  //Checks if Sensor 1 is available
int bme2Detected = 0;                                  //Checks if Sensor 2 is available

#define ONE_WIRE_BUS 2                                 // Data wire is plugged into port 2 on the Arduino
OneWire oneWire(ONE_WIRE_BUS);                         // Setup a oneWire instance to communicate with any OneWire devices
DallasTemperature sensors(&oneWire);                   // Pass our oneWire reference to Dallas Temperature.

////////////////////////////////////////1////////////////////////////////////////////////////////
//                                           Setup                                             //
////////////////////////////////////////////////////////////////////////////////////////////////
void sensor_setup()
{
  Wire.begin();
  sensors.begin();
  bme1.parameter.communication = 0;                    //I2C communication for Sensor 1 (bme1)
  bme2.parameter.communication = 0;                    //I2C communication for Sensor 2 (bme2)
  bme1.parameter.I2CAddress = 0x77;                    //I2C Address for Sensor 1 (bme1)
  bme2.parameter.I2CAddress = 0x76;                    //I2C Address for Sensor 2 (bme2)
  bme1.parameter.sensorMode = 0b11;                    //Setup Sensor mode for Sensor 1
  bme2.parameter.sensorMode = 0b11;                    //Setup Sensor mode for Sensor 2
  bme1.parameter.IIRfilter = 0b100;                    //IIR Filter for Sensor 1
  bme2.parameter.IIRfilter = 0b100;                    //IIR Filter for Sensor 2
  bme1.parameter.humidOversampling = 0b101;            //Humidity Oversampling for Sensor 1
  bme2.parameter.humidOversampling = 0b101;            //Humidity Oversampling for Sensor 2
  bme1.parameter.tempOversampling = 0b101;              //Temperature Oversampling for Sensor 1
  bme2.parameter.tempOversampling = 0b101;              //Temperature Oversampling for Sensor 2
  bme1.parameter.pressOversampling = 0b101;             //Pressure Oversampling for Sensor 1
  bme2.parameter.pressOversampling = 0b101;             //Pressure Oversampling for Sensor 2
  bme1.parameter.pressureSeaLevel = 1013.25;            //default value of 1013.25 hPa (Sensor 1)
  bme2.parameter.pressureSeaLevel = 1013.25;            //default value of 1013.25 hPa (Sensor 2)
  bme1.parameter.tempOutsideCelsius = 15;               //default value of 15°C
  bme2.parameter.tempOutsideCelsius = 15;               //default value of 15°C
  bme1.parameter.tempOutsideFahrenheit = 59;            //default value of 59°F
  bme2.parameter.tempOutsideFahrenheit = 59;            //default value of 59°F
  bme1.init();                                          //First BME280 Sensor detected
  bme2.init();                                          //Second BME280 Sensor detected!

}
////////////////////////////////////////1////////////////////////////////////////////////////////
//                                           Loop                                             //
////////////////////////////////////////////////////////////////////////////////////////////////
void sensor_loop()
{

  ////////////////////////////////////////1////////////////////////////////////////////////////////
  //                Strom und Spannung messen und berechnen                                      //
  ////////////////////////////////////////////////////////////////////////////////////////////////

  //Spannung V lesen
  adc0 = ads.readADC_SingleEnded(0);
  //FiltVal(adc0,);

  Spannung = map(adc0, 0, 6640, 0, 1380);
  if (adc0 <= 0)Spannung = 0;                              //Anzeige auf Null wenn das Netzteil aus ist
  Spannung = Spannung / 100;



  //Strom I lesen
  adc1 = ads.readADC_SingleEnded(1);
  Strom = map(adc1, 180, 2027, 0, 5000);
  if (adc1 <= 185) Strom = 0;                               //Anzeige auf Null wenn das Netzteil aus ist
  Strom = Strom / 100;


  // Leistung W berechnen
  Leistung = Strom * Spannung;


  ////////////////////////////////////////1////////////////////////////////////////////////////////
  //                Serielle Ausgabe zur kalibrierung von Volt und Ampere                       //
  ////////////////////////////////////////////////////////////////////////////////////////////////
  //
  Serial.print(adc0);
  Serial.print("\n\r");
  Serial.print("\n\r");
  Serial.print(adc1);
  Serial.print("\n\r\n\r");
  //  delay(300);
  //
  ////////////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////1////////////////////////////////////////////////////////
  //                        Temperatur der OneWire Sensoren lesen                               //
  ////////////////////////////////////////////////////////////////////////////////////////////////

  sensors.requestTemperatures(); // Send the command to get temperatures
  //  Serial.println(sensors.getTempCByIndex(0));
  //  Serial.println(sensors.getTempCByIndex(1));

  ////////////////////////////////////////1////////////////////////////////////////////////////////
  //                        Temperatur und mehr der BME Sensoren lesen                          //
  ////////////////////////////////////////////////////////////////////////////////////////////////

  //  Serial.print(F("Temperature Sensor 1 [°C]:\t\t"));
  //  Serial.println(bme1.readTempC());
  //  Serial.print(F("Humidity Sensor 1 [%]:\t\t\t"));
  //  Serial.println(bme1.readHumidity());
  //  Serial.print(F("Pressure Sensor 1 [hPa]:\t\t"));
  //  Serial.println(bme1.readPressure());
  //  Serial.print(F("Altitude Sensor 1 [m]:\t\t\t"));
  //  Serial.println(bme1.readAltitudeMeter());
  //  Serial.println(F("****************************************"));
  //
  //  Serial.print(F("Temperature Sensor 2 [°C]:\t\t"));
  //  Serial.println(bme2.readTempC());
  //  Serial.print(F("Humidity Sensor 2 [%]:\t\t\t"));
  //  Serial.println(bme2.readHumidity());
  //  Serial.print(F("Pressure Sensor 2 [hPa]:\t\t"));
  //  Serial.println(bme2.readPressure());
  //  Serial.print(F("Altitude Sensor 2 [m]:\t\t\t"));
  //  Serial.println(bme2.readAltitudeMeter());
  //
  //  Serial.println();
  //  Serial.println();

}

////////////////////////////////////////1////////////////////////////////////////////////////////
//                        Funktion zum glätten der Werte                                      //
////////////////////////////////////////////////////////////////////////////////////////////////

void Filtern(int16_t &FiltVal, int16_t NewVal, int FF) {
  FiltVal = ((FiltVal * FF) + NewVal) / (FF + 1);
}

////////////////////////////////////////1////////////////////////////////////////////////////////
//                        Funktion um aus uint16_t Kommazahl zu bilden                        //
/////////////////////////////////////////////////////////////////////////////////////////////////
