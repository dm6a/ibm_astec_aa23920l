#include "TouchScreen.h"

#define YP A0  // must be an analog pin, use "An" notation!
#define XM A1  // must be an analog pin, use "An" notation!
#define YM 8   // can be a digital pin
#define XP 7   // can be a digital pin


// For better pressure precision, we need to know the resistance
// between X+ and X- Use any multimeter to read it
// For the one we're using, its 300 ohms across the X plate
TouchScreen ts = TouchScreen(XP, YP, XM, YM, 289);

void touch_setup() {
  //Pipser für Tastentöne an pin 5
  //DDRD |= (1<<PD5); //Pin als Output
  digitalWrite(5, LOW);//PORTD  &= ~(1<<PD5); // Pin LOW
}

void touch_loop() {
  TSPoint p = ts.getPoint();

  //Exit
  if (p.z > ts.pressureThreshhold) {
    if (p.x > 335 && p.x < 403 && p.y < 847 && p.y > 769) {

      sh_volt  = 0;
      sh_amp  = 0;
      sh_watt  = 0;
      sh_big = 0;
      sh_change = 1;
      digitalWrite(5, HIGH);//PORTD |= (1<<PD5); //Port HIGH
    }

    //VOLT in gross anzeigen
    if (p.x > 335 && p.x < 396 && p.y < 740 && p.y > 656) {

      sh_volt  = 1;
      sh_change = 1;
      sh_big = 1;
      sh_watt  = 0;
      sh_amp  = 0;
      digitalWrite(5, HIGH);//PORTD |= (1<<PD5); //Port HIGH
    }

    //AMP in gross anzeigen
    if (p.x > 331 && p.x < 399 && p.y < 619 && p.y > 395) {

      sh_amp  = 1;
      sh_change = 1;
      sh_big = 1;
      sh_volt  = 0;
      sh_watt  = 0;
      digitalWrite(5, HIGH);//PORTD |= (1<<PD5); //Port HIGH
    }

    //WATT in gross anzeigen
    if (p.x > 331 && p.x < 398 && p.y < 520 && p.y > 432) {

      sh_watt  = 1;
      sh_change = 1;
      sh_big = 1;
      sh_volt  = 0;
      sh_amp  = 0;
      digitalWrite(5, HIGH); //PORTD |= (1<<PD5); //Port HIGH
    }

    //Umschalten zwischen 12 und 13.8 Volt Output
    if (p.x > 342 && p.x < 405 && p.y < 410 && p.y > 322) {
      if (V12_13) {
        V12_13 = 0;
        OUTPUT_CH = 1;
        digitalWrite(5, HIGH);//PORTD |= (1<<PD5); //BEEP
        ON_OFF_CH = 1;
      }
      else {
        V12_13 = 1;
        OUTPUT_CH = 1;
        digitalWrite(5, HIGH);//PORTD |= (1<<PD5); //BEEP
        ON_OFF_CH = 1;
      }
    }

    //Netzteil ON OFF
    if (p.x > 333 && p.x < 402 && p.y < 297 && p.y > 218) {
      if (pson_t == 1) {
        pson_t = 0;
        ON_OFF_CH = 1;
        digitalWrite(6, LOW);//PORTD &= ~(1<<PD6);
        digitalWrite(5, HIGH); //Port HIGH
      }
      else {
        pson_t = 1;
        digitalWrite(5, HIGH); //PORTD |= (1 << PD5); // Pin HIGH
        digitalWrite(6, HIGH);//PORTD |= (1 << PD6);
        ON_OFF_CH = 1;
      }

    }
    delay(100);
    digitalWrite(5, LOW);//PORTD &= ~(1<<PD5); //Port LOW
    delay(400);

  }
  ////////////////////////////////////////1////////////////////////////////////////////////////////
  //                         Ausgabe des Threshscreen zum einrichten                             //
  ////////////////////////////////////////////////////////////////////////////////////////////////
  //TSPoint p = ts.getPoint();
  // we have some minimum pressure we consider 'valid'
  //  pressure of 0 means no pressing!
  //  if (p.z > ts.pressureThreshhold) {
  //    Serial.print("X = "); Serial.print(p.x);
  //    Serial.print("\tY = "); Serial.print(p.y);
  //    Serial.print("\tPressure = "); Serial.println(p.z);
  //  }
}
