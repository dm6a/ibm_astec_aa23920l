
void Graph( double x, double y, double gx, double gy, double w, double h, double xlo, double xhi, double xinc, double ylo, double yhi, double yinc, String title, String xlabel, String ylabel, unsigned int gcolor, unsigned int acolor, unsigned int pcolor, unsigned int tcolor, unsigned int bcolor, boolean &redraw) {
  tft.setFont();
  double ydiv, xdiv;
  // initialize old x and old y in order to draw the first point of the graph
  // but save the transformed value
  // note my transform funcition is the same as the map function, except the map uses long and we need doubles
  //static double ox = (x - xlo) * ( w) / (xhi - xlo) + gx;
  //static double oy = (y - ylo) * (gy - h - gy) / (yhi - ylo) + gy;
  double i;
  double temp;
  int rot, newrot;
  if (redraw == true) {
    redraw = false;
//Mit Schwarz füllen und alles überschreiben
    tft.fillRect(9, 90, 466, 164, BGCOLOR);

    ox = (x - xlo) * ( w) / (xhi - xlo) + gx;
    oy = (y - ylo) * (gy - h - gy) / (yhi - ylo) + gy;
    // draw y scale
    for ( i = ylo; i <= yhi; i += yinc) {
      // compute the transform
      temp =  (i - ylo) * (gy - h - gy) / (yhi - ylo) + gy;

      if (i == 0) {
        tft.drawLine(gx, temp, gx + w, temp, acolor);
      }
      else {
        tft.drawLine(gx, temp, gx + w, temp, gcolor);
      }

      tft.setTextSize(0);
      tft.setTextColor(tcolor, bcolor);
      tft.setCursor(gx - 40, temp);
      // precision is default Arduino--this could really use some format control
      tft.println(i);
    }
    // draw x scale
    for (i = xlo; i <= xhi; i += xinc) {

      // compute the transform

      temp =  (i - xlo) * ( w) / (xhi - xlo) + gx;
      if (i == 0) {
        tft.drawLine(temp, gy, temp, gy - h, acolor);
      }
      else {
        tft.drawLine(temp, gy, temp, gy - h, gcolor);
      }

      tft.setTextSize(0);
      tft.setTextColor(tcolor, bcolor);
      tft.setCursor(temp, gy + 10);
      // precision is default Arduino--this could really use some format control
      tft.println(i,0);
    }

    //now draw the labels
//    tft.setTextSize(0);
//    tft.setTextColor(tcolor, bcolor);
//    tft.setCursor(gx , gy - h - 30);
//    tft.println(title);
//
//    tft.setTextSize(0);
//    tft.setTextColor(acolor, bcolor);
//    tft.setCursor(gx , gy + 20);
//    tft.println(xlabel);

    tft.setTextSize(0);
    tft.setTextColor(acolor, bcolor);
    tft.setCursor(gx - 30, gy - h - 10);
    tft.println(ylabel);


  }

  //graph drawn now plot the data
  // the entire plotting code are these few lines...
  // recall that ox and oy are initialized as static above
  x =  (x - xlo) * ( w) / (xhi - xlo) + gx;
  y =  (y - ylo) * (gy - h - gy) / (yhi - ylo) + gy;
  tft.drawLine(ox, oy, x, y, pcolor);
  tft.drawLine(ox, oy + 1, x, y + 1, pcolor);
  tft.drawLine(ox, oy - 1, x, y - 1, pcolor);
  ox = x;
  oy = y;

}

/*
  End of graphing functioin
*/
