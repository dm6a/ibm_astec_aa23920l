#include <Wire.h>
byte Netzteil_Address = 0x50;               //Adresse vom Netzteil ADM1041

void pwrsup_setup() {
  Wire.begin();

  //Port 6 Netzteil On Off
  //DDRD |= (1 << PD6); //Pin als Output
  digitalWrite(6, LOW); //PORTD  &= ~(1 << PD6); // Pin LOW um das Netzteil anzuschalten
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                          Funktion um auf den Bus zu schreiben und zu lesen                               //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void i2c_schreiben(byte adr_w, byte reg_w, byte val_w)
{
  Wire.beginTransmission(adr_w);
  Wire.write(reg_w);
  Wire.write(val_w);
  Wire.endTransmission();

  delay(70);
}

void i2c_lesen(byte adr_r, byte reg_r, byte val_r)
{

  Wire.beginTransmission(adr_r);
  Wire.write(reg_r);
  Wire.endTransmission();
  Wire.requestFrom(adr_r, 1);

  byte blabla = Wire.read();
  delay(70);
}

void pwrsup_loop()
{
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //                                         Netzteil auf 13,8V ändern                                          //
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  if (OUTPUT_CH) {
    digitalWrite(6, LOW);// PORTD |= (1 << PD6);
    pson_t = 0;
    OUTPUT_CH = 0;
    if (V12_13 == 0) {

      digitalWrite(6, LOW);// PORTD |= (1 << PD6); //Netzteil abschalten bevor die Spannung geändert wird
      Serial.println("12 000");
      loadValue = 0x63;
      ovpValue = 0x99;

      //Werte scheiben auf dem Bus
      i2c_schreiben(Netzteil_Address, loadVoltReg, loadValue);
      i2c_schreiben(Netzteil_Address, ovpReg, ovpValue);
    }
    else {
      Serial.println("13 000");
      digitalWrite(6, LOW);//PORTD |= (1 << PD6); //Netzteil abschalten bevor die Spannung geändert wird
      loadValue = 0x1F;
      ovpValue = 0x75;

      //Ausgangsspannung änder
      i2c_schreiben(Netzteil_Address, loadVoltReg, loadValue);
      i2c_schreiben(Netzteil_Address, ovpReg, ovpValue);
    }


  }

}




















// int Usensor = analogRead(A3);
// Filtern(FiltVal, Usensor, 10); //Werte glätten
// Serial.println(FiltVal);
// MapFiltVal = map(FiltVal, 530.00, 906.00, 0.00, 50.00);
// Serial.println(MapFiltVal);
//MapFiltVal = 50.00;
