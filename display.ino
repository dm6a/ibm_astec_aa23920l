#include <SPI.h>
#include "Adafruit_GFX.h"
#include "Adafruit_HX8357.h"

// These are 'flexible' lines that can be changed
#define TFT_CS 10
#define TFT_DC 9
#define TFT_RST -1 // War mal auf 8 RST can be set to -1 if you tie it to Arduino's reset

// Colors
#define WHITE     0xFFFF
#define BLACK     0x0000
#define YELLOW    0xFFE0
#define DKGREEN   0x03E0
#define GREY      0xA618 //bissel blau 
#define RED      0xF800
#define GREEN    0x07E0
#define TEXTCOLOR 0xFFFF
#define BGCOLOR  0x0000
#define BUTCOLOR1 0x7BCF
#define BUTCCOLOR 0x7BFF
#define BUBORDER1 0xCE59
#define BUBORDER2 0x39E7
#define BARGREEN  0x07E0
#define BARRED  0xF800
#define BARACT 0x07E0
#include <Fonts/FreeSans24pt7b.h>
#include <Fonts/FreeSans9pt7b.h>

// Use hardware SPI (on Uno, #13, #12, #11) and the above for CS/DC
Adafruit_HX8357 tft = Adafruit_HX8357(TFT_CS, TFT_DC, TFT_RST);

void display_main_setup()
{

  tft.begin(HX8357D);

  //Display Drehen
  tft.setRotation(3);

  //Intro DK8DE
  //Schwarzer Bildschirm zum Start
  tft.fillScreen(BGCOLOR);

  //Outline Display
  tft.drawRect(0, 0, 480, 320, BUBORDER1);
  tft.drawRect(1, 1, 478, 318, BUBORDER1);

  //************************************************************************
  //        Debug um beim BU Exit die Schrift nicht zu verschieben
  //***********************************************************************
  tft.setCursor(55, 20);
  tft.setTextColor(BGCOLOR);
  tft.setFont(&FreeSans24pt7b);
  tft.print(".");
  //************************************************************************

  //Intro DK8DE
  if (intro) {
    tft.setCursor(55, 135);
    tft.setTextSize(1);
    tft.setTextColor(TEXTCOLOR);
    tft.print("12V 3880W PSU");
    tft.setCursor(165, 200);
    tft.print("DK8DE");
    tft.setTextSize(0);
    delay(3000);
  }
  //Trennlinien
  tft.drawLine(2, 64, 480, 64, BUBORDER1);
  tft.drawLine(2, 256, 480, 256, BUBORDER1);

  //BU Exit
  drbu(10, 10, 65, 46);
  tft.setCursor(23, 38);
  tft.setTextColor(TEXTCOLOR);
  tft.setFont(&FreeSans9pt7b);
  tft.print("EXIT");

  //BU Volt
  drbu(89, 10, 65, 46);
  tft.setCursor(98, 38);
  tft.print("VOLT");

  //BU AMP
  drbu(168, 10, 65, 46);
  tft.setCursor(182, 38);
  tft.print("AMP");

  //BU Watt
  drbu(247, 10, 65, 46);
  tft.setCursor(254, 38);
  tft.print("WATT");

  //BU auf 13,8 V umstellen
  drbu(326, 10, 65, 46);
  tft.setCursor(331, 38);
  tft.print("13.8 V");


  //BU ON
  drbu(405, 10, 65, 46);
  tft.setCursor(424, 38);
  tft.print("ON");


  //Output V
  tft.setCursor(20, 280);
  tft.setTextColor(TEXTCOLOR);
  tft.setFont(&FreeSans9pt7b);
  tft.print("Mainvol:");

  //Power ON / OFF
  tft.setCursor(20, 303);
  tft.print("Power:");

  //Temp In
  tft.setCursor(175, 280);
  tft.setTextColor(TEXTCOLOR);
  tft.print("Temp IN:");
  tft.setTextColor(GREEN);
  tft.setCursor(297, 280);
  tft.print("C");

  //Temp OUT
  tft.setCursor(175, 303);
  tft.setTextColor(TEXTCOLOR);
  tft.print("Temp OUT:");
  tft.setTextColor(GREEN);
  tft.setCursor(297, 303);
  tft.print("C");

  //V Out
  tft.setCursor(330, 280);
  tft.setTextColor(TEXTCOLOR);
  tft.print("Out:");


  //Status / Fehler
  tft.setCursor(330, 303);
  tft.print("Err:");

}

//Funktion um die Buttons zu Zeichnen
void drbu(int A1, int A2, int A3, int A4)
{
  //tft.drawRect(A1 , A2, A3 + 1, A4 + 1, BUBORDER2);
  tft.drawRect(A1 - 1, A2 - 1 , A3, A4, BUBORDER1);
  tft.fillRect(A1, A2, A3, A4, BUTCOLOR1);
}

void display_main_loop()
{
  //Sammeln und berechnen von Messdaten

  //Main V 230V
  //  if (Vout_alt != Vout)
  // {
  tft.setFont(&FreeSans9pt7b);
  tft.setCursor(81, 280);
  //tft.setTextColor(BGCOLOR);
  //tft.print("230.00");
  //tft.setCursor(81, 280);
  tft.setTextColor(GREEN);
  tft.print("230.00");
  // Vout_alt = Vout;
  tft.setCursor(135, 280);
  tft.print(" V");
  // }
  //Werte auf 1 damit bei keine Änderung der Messdaten die Zahlen dargestellt werden
  if (sh_change == 1) {
    AlteSpannung = 1;
    AlterStrom = 1;
    AlteLeistung = 1;
  }

  //Power ON / OFF

  if (pson_t && ON_OFF_CH == 1)
  {
    tft.setFont(&FreeSans9pt7b);
    tft.setCursor(81, 303);
    tft.setTextColor(BGCOLOR);
    tft.print("OFF");
    tft.setCursor(81, 303);
    tft.setTextColor(GREEN);
    tft.print("ON");
    no_fault = 1; //Setzt den Fehler zurück, sollte das Netzteil durch einen Fehler abgeschaltet worden sein
    ON_OFF_CH = 0;
  }
  if (pson_t == 0 && ON_OFF_CH == 1)
  {
    tft.setFont(&FreeSans9pt7b);
    tft.setTextColor(BGCOLOR);
    tft.setCursor(81, 303);
    tft.print("ON");
    tft.setCursor(81, 303);
    tft.setTextColor(RED);
    tft.print("OFF");
    ON_OFF_CH = 0;
  }

  //Temp In
  if (temp1 != temp2)
  {
    tft.setFont(&FreeSans9pt7b);
    tft.setCursor(275, 280);
    tft.setTextColor(BGCOLOR);
    tft.print(temp1);
    tft.setCursor(275, 280);
    tft.setTextColor(GREEN);
    tft.print(temp1);
    temp1 = temp1;
  }


  //Temp OUT
  if (temp1 != temp2)
  {
    tft.setFont(&FreeSans9pt7b);
    tft.setCursor(275, 303);
    tft.setTextColor(BGCOLOR);
    tft.print(temp2);
    tft.setCursor(275, 303);
    tft.setTextColor(GREEN);
    tft.print(temp2);
    temp2 = temp1;
  }

  //Umschaltung auf 13.8 V und 12.00 V
  if (OUTPUT_CH == 1) {
    if (V12_13 == 1)
    {
      tft.setFont(&FreeSans9pt7b);
      tft.setCursor(375, 280);
      tft.setTextColor(BGCOLOR);
      tft.print("12.00 V");
      tft.setTextColor(RED);
      tft.setCursor(375, 280);
      tft.print("13.80 V");
    }
    else
    {
      tft.setFont(&FreeSans9pt7b);
      tft.setCursor(375, 280);
      tft.setTextColor(BGCOLOR);
      tft.print("13.80 V");
      tft.setTextColor(GREEN);
      tft.setCursor(375, 280);
      tft.print("12.00 V");
    }
  }
  //Status / Fehler
  /*  if ( controlStatusRegister &  (1 << OVER_IN_TEM) ) {
      fault = "OT FAULT";
      fault_alt = 1;
      no_fault = 0;
      pson_t = 0; //Schaltet beim Fehler das Netzteil aus
    }
    else if (controlStatusRegister &  (1 << OV_TRIP) ) {
      fault = "OV FAULT";
      fault_alt = 1;
      no_fault = 0;
      pson_t = 0; //Schaltet beim Fehler das Netzteil aus
    }
    else if (controlStatusRegister &  (1 << OC_TRIP) ) {
      fault = "OC FAULT";
      fault_alt = 1;
      no_fault = 0;
      pson_t = 0; //Schaltet beim Fehler das Netzteil aus
    }*/
  // else {
  fault_alt = 0;
  fault = "Normal";
  //  }
  if (fault_alt == 1)
  {
    tft.setFont(&FreeSans9pt7b);
    tft.setCursor(375, 303);
    tft.setTextColor(BGCOLOR);
    tft.print("Normal");
    tft.setCursor(375, 303);
    tft.setTextColor(RED);
    tft.print(fault);
  }
  else if (no_fault == 1)
  {
    tft.setFont(&FreeSans9pt7b);
    tft.fillRect(370, 285, 105, 25, BGCOLOR);
    tft.setCursor(375, 303);
    tft.setTextColor(GREEN);
    tft.print("Normal");
    fault_alt = 0;
    no_fault = 0;
  }
  //Anzeige in der Mitte ueberschreiben und Zahlen in gross anzeigen

  if (sh_change == 1) {
    tft.fillRect(9, 65, 466, 184, BGCOLOR);
  }
  if (sh_big != 1) {
    //Sensor U I P
    //V
    if (Spannung != AlteSpannung)
    {
      tft.setFont(&FreeSans24pt7b);
      tft.setCursor(250, 111);
      tft.setTextColor(BGCOLOR);
      tft.print(AlteSpannung);
      tft.setCursor(250, 111);
      tft.setTextColor(TEXTCOLOR);
      tft.print(Spannung);
      AlteSpannung = Spannung;
    }

    //A
    if (Strom != AlterStrom)
    {
      tft.setFont(&FreeSans24pt7b);
      tft.setCursor(250, 175);
      tft.setTextColor(BGCOLOR);
      tft.print(AlterStrom);
      tft.setCursor(250, 175);
      tft.setTextColor(TEXTCOLOR);
      tft.print(Strom);
      AlterStrom = Strom;
    }

    //WATT
    if (Leistung != AlteLeistung)
    {
      tft.setCursor(250, 239);
      tft.setFont(&FreeSans24pt7b);
      tft.setTextColor(BGCOLOR);
      tft.print(AlteLeistung);
      tft.setCursor(250, 239);
      tft.setTextColor(TEXTCOLOR);
      tft.print(Leistung);
      AlteLeistung = Leistung;
    }
    //Bar V1
    //      tft.fillRect(10, 80, 179, 15, BARRED);
    //      tft.fillRect(189, 80, 23, 15, BARYELLOW);
    //      tft.fillRect(212, 80, 23, 15, BARGREEN);
    tft.drawRect(9, 80, 227, 34, BUBORDER1);

    //Bar I1
    //      tft.fillRect(10, 144, 179, 15, BARGREEN);
    //      tft.fillRect(189, 144, 23, 15, BARYELLOW);
    //      tft.fillRect(212, 144, 23, 15, BARRED);
    tft.drawRect(9, 144, 227, 34, BUBORDER1);

    //Bar W1
    //      tft.fillRect(10, 208, 179, 15, BARGREEN);
    //      tft.fillRect(189, 208, 23, 15, BARYELLOW);
    //      tft.fillRect(212, 208, 23, 15, BARRED);
    tft.drawRect(9, 208, 227, 34, BUBORDER1);

    //Bar V2
    tft.fillRect(10 + ((Spannung / 12000) * 225), 81, 225 - ((Spannung / 12000) * 225), 32, BLACK);
    tft.fillRect(10, 81, ((Spannung / 12000) * 225), 32, BARACT);   //der soll sich bewegen bzw. Läge ändern für U.

    //Bar I2
    tft.fillRect(10 + ((Strom / 57000) * 225), 145, 225 - ((Strom / 57000) * 225), 32, BLACK);
    tft.fillRect(10, 145, ((Strom / 57000) * 225), 32, BARACT);   //der soll sich bewegen bzw. Läge ändern für I

    //Bar W2
    tft.fillRect(10 + ((Leistung / 2950000) * 209), 209, 225 - ((Leistung / 2950000) * 225), 32, BLACK);
    tft.fillRect(10, 209, ((Leistung / 2950000) * 209), 32, BARACT);    //der soll sich bewegen bzw. Läge ändern für P

    //Symbol rechts mit U / I / P
    tft.setTextColor(TEXTCOLOR);
    tft.setFont(&FreeSans24pt7b);
    tft.setCursor(430, 111);
    tft.print("V");

    //A
    tft.setCursor(430, 175);
    tft.print("A");

    //WATT
    tft.setCursor(424, 239);
    tft.print("W");
  }
  else {
    /*
       Graph neu aufbauen wenn 60 sec vergangen sind
    */
    if (x_delay_graph >= 60 || sh_change == 1) {
      display1 = true; //Anzeige zurücksetzen
      x_delay_graph = 0; //Sekundenzähler zurücksetzen
      x = 0;
      y = 0;
      ox = 0;
      oy = 0;
    }
    else {
      x = x_delay_graph;
    }
    //VOLT in Groß darstellen
    if (sh_volt == 1) {
      /*
        Graph darstellen für Strom. Nach 60 Sec wird er neu aufgebaut
      */
      y = Spannung; //Keine anpassung der Scala da nur max 12V
      Graph(x, y, 50, 230, 405, 130, 0, 60, 10, 0, 16, 2, "", "", "VOLT", GREY, WHITE, YELLOW, WHITE, BLACK, display1);
    }
    //AMP in Groß darstellen
    if (sh_amp == 1) {
      /*
        Graph darstellen für Strom. Nach 60 Sec wird er neu aufgebaut
      */
      y = Strom; //Keine anpassung der Scala da nur max 200A
      Graph(x, y, 50, 230, 405, 130, 0, 60, 10, 0, 200, 20, "", "", "AMP", GREY, WHITE, YELLOW, WHITE, BLACK, display1);
    }
    if (sh_watt == 1) {
      /*
         Graph darstellen für Watt. Nach 60 Sec wird er neu aufgebaut
      */
      y = Leistung / 1000; //anpassen an die Scala bis 3.0 KW geht in 0.25KW Schritten
      Graph(x, y, 50, 230, 405, 130, 0, 60, 10, 0, 3, 0.25, "", "", "KW", GREY, WHITE, YELLOW, WHITE, BLACK, display1);
    }
    /*
       Volt Amp und Watt im über dem Graphen darstellen
    */
    if (Spannung != AlteSpannung)
    {
      FONT_COLOR = 0xFFFF;
      tft.setFont(&FreeSans9pt7b);
      tft.setTextSize(0);
      tft.setTextColor(BGCOLOR);
      tft.setCursor(110, 89);
      tft.print(AlteSpannung);
      tft.setTextColor(FONT_COLOR);
      tft.setCursor(90, 89);
      tft.print("V");
      tft.setCursor(110, 89);
      tft.print(Spannung);

      AlteSpannung = Spannung;
    }

    if (Strom != AlterStrom)
    {
      tft.setFont(&FreeSans9pt7b);
      tft.setTextSize(0);
      tft.setTextColor(FONT_COLOR);
      tft.setCursor(170, 89);
      tft.print("A");
      tft.setCursor(185, 89);
      tft.setTextColor(BGCOLOR);
      tft.print(AlterStrom);
      tft.setCursor(185, 89);
      tft.setTextColor(FONT_COLOR);
      tft.print(Strom);
      AlterStrom = Strom;
    }

    if (Leistung != AlteLeistung)
    {
      tft.setFont(&FreeSans9pt7b);
      tft.setTextColor(FONT_COLOR);
      tft.setTextSize(0);
      tft.setCursor(250, 89);
      tft.print("W");
      tft.setCursor(270, 89);
      tft.setTextColor(BGCOLOR);
      tft.print(AlteLeistung);
      tft.setCursor(270, 89);
      tft.setTextColor(FONT_COLOR);
      tft.print(Leistung);
      AlteLeistung = Leistung;
    }
  }
  sh_change = 0;
}
